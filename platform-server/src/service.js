import express from 'express';
import cors from 'cors';
import path from 'path';
import { emitter } from './emitter';
import { authenticate } from './authenticate';

const racket = express();

racket.use(express.json());

// set the view engine to ejs
racket.set('view engine', 'ejs');
racket.set('views', path.resolve(__dirname, '..', 'views'));

racket.use(cors({
    allowedHeaders: [
        'X-univ-api-key'
    ],
    exposedHeaders: [
        'X-univ-api-key'
    ]
}));

racket.get('/', (req, res) => {
    return res.render('index');
});

racket.use('/', authenticate(racket) , emitter());

export {
    racket
}
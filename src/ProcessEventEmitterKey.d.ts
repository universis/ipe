export class ProcessEventEmitterKey {
    async read(): Promise<string[]>;
    async readOne(): Promise<string>;
}
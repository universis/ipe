
import * as superagent from 'superagent';
import { ProcessEventEmitterKey } from './ProcessEventEmitterKey';

class ProcessEventSubscription {
    constructor(emitter) {
        this.emitter = emitter;
    }
    unsubscribe() {
        this.emitter.unsubscribe();
    }
}

class ProcessEventEmitter {
    listener;
    /**
     * @type {import('superagent').Response}
     */
    channel;
    /**
     * @param {string=} serviceHost 
     */
    constructor(serviceHost) {
        this.serviceHost = serviceHost || 'http://localhost:14400/';
        this.options = {
            key: null
        }
    }

    async emit(value) {
        if (this.options.key == null) {
            this.options.key = await new ProcessEventEmitterKey().readOne();
        }
        const key = this.options.key;
        const emitURL = new URL(`emit?key=${encodeURIComponent(key)}`, this.serviceHost).toString();
        await superagent
            .post(emitURL)
            .send(value);
    }

    async getKey() {
        if (this.options.key != null) {
            return this.options.key;
        }
        this.options.key = await new ProcessEventEmitterKey().readOne();
        return this.options.key;
    }

    ping() {
        return new Promise((resolve, reject) => {
            const subscribeURL = new URL(`subscribe?key=${encodeURIComponent(key)}`, this.serviceHost).toString();
            superagent.options(subscribeURL, (err) => {
                if (err) {
                    reject(err);
                }
                resolve(true);
            });
        });
    }

    watch(onError) {

        this.getKey().then((key) => {
            const subscribeURL = new URL(`subscribe?key=${encodeURIComponent(key)}`, this.serviceHost).toString();
            // validate subscription by send an OPTIONS request
            superagent.options(subscribeURL, (err) => {
                if (err) {
                    // if error, use error callback
                    if (typeof onError === 'function') {
                        onError(err);
                    }
                    // and exit
                    return;
                }
                // subcribe with buffer enabled and parse request
                void superagent.get(subscribeURL).buffer(true).parse((res) => {
                    this.channel = res;
                    let chunks = [];
                    // use data event and get message in chunks
                    res.on('data',  (chunk) => {
                        const body = Buffer.from(chunk).toString();
                        // on message end
                        if (body === '\n\n') {
                            let event = null;
                            if (chunks.length > 0) {
                                const message = chunks.join().replace(/^data:\s/, '').replace(/\'$/, '');
                                // clear chunks
                                chunks = [];
                                // and parse message string
                                event = JSON.parse(message);
                            }
                            // send event to lister
                            this.listener(event);
                        } else {
                            // otherwise append chunk
                            chunks.push(body);
                        }
                    });
                }).then(() => {
                    //
                }).catch((err) => {
                    // if error, use error callback
                    if (typeof onError === 'function') {
                        onError(err);
                    }
                });
            });
            // continue watching
            this.watching(onError);
        });

        
    }

    subscribe(next, onError) {
        // set listener
        this.listener = (event) => {
            void next(event);
        };
        // try to subscribe and use error callback if any
        this.watch(onError);
        // and return subscrtiion
        return new ProcessEventSubscription(this);
    }

    watching(onError) {
        if (this.heartbeatHandle) {
            clearInterval(this.heartbeatHandle);
            delete this.heartbeatHandle;
        }
        this.heartbeatHandle = setInterval(() => {
            // destroy channel
            if (this.channel) {
                this.channel.destroy();
            }
            this.watch(onError);
        }, 120000);
    }

    unsubscribe() {
        if (this.channel) {
            this.channel.destroy();
        }
        if (this.heartbeatHandle) {
            clearInterval(this.heartbeatHandle);
            delete this.heartbeatHandle;
        }
    }
}

export {
    ProcessEventSubscription,
    ProcessEventEmitter
}
#!/usr/bin/env node
const { serve } = require('./dist');
const { Command, Option } = require('commander');
const program = new Command();
const packageJson = require('../package.json')
program
    .name('npx @universis/racket')
    .description('starts @universis/racket service')
    .version(packageJson.version)
    .addOption(new Option('-p, --port <number>', 'port number'))
    .addOption(new Option('-h, --host <string>', 'host address'))
program.parse();
const args = program.opts();
serve(args.port, args.host);
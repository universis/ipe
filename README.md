# @universis/racket

An inter-process event service which implements the communication between processes by sending and receiving messages in an active inter-connector environment.


![@universis/racket](https://gitlab.com/universis/racket/-/raw/main/diagram.png?inline=true)

This implementation is very important for applications and services which are running in cluster mode and every cluster worker should inform cluster for something.

## Install

    npm i @universis/racket

## Usage

### InterProcessEventEmitter

`InterProcessEventEmitter` handles process events and emits them even if process is running in cluster mode or under [pm2](https://github.com/Unitech/pm2). Each worker of a cluster or the process itself has the option to handle inter-process events by using `InterProcessEventEmitter.subscribe()`.

Register globally `InterProcessEventEmitter` handler:

    import { InterProcessEventEmitter } from './InterProcessEventEmitter';
    InterProcessEventEmitter.register();

or use `@universis/racket/register` as `--require` script under nodejs

    node --require @universis/racket/register myapp.js

Start sending process events

    import { InterProcessEventEmitter } from '@universis/racket';

    new InterProcessEventEmitter().emit({
        body: 'Hello World!'
    });

or subscribing for new events

    import { InterProcessEventEmitter } from '@universis/racket';

    const emitter = new InterProcessEventEmitter().subscribe((message) => {
        // write your code here
    });

    ...
    emitter.unsubscribe();

### ProcessEventEmitter

`ProcessEventEmitter` uses `@universis/racket` service to send and receive process events. 
`@universis/racket` event service is an HTTP service which acts as a proxy of all events emitted by any client of that service. Process events are being collected by the service and transmitted to any client that has been subscribed for receiving messages.

Start `@universis/racket/platform-server` as a standalone service

    npx @universis/racket/platform-server/register

or register `@universis/racket/platform-server` under pm2

    pm2 start @universis/racket/platform-server/register -n racket

This operation will start an instance of `@universis/racket` service which is a typical HTTP service and starts listening for process subscription under `http://localhost:14440`

#### ProcessEventEmitter.subscribe

Any process may use `ProcessEventEmitter` class and subscribe for new events:

    import { ProcessEventEmitter } from '@universis/racket';

    const subscription = new ProcessEventEmitter().subscribe((event) => {
        // write your code here
    });

    // unsubscribe
    subscription.unsubscribe();

When service has been configured to run in different port use `serviceHost` param to define service host:

    import { ProcessEventEmitter } from '@universis/racket';

    const subscription = new ProcessEventEmitter('http://localhost:12345/').subscribe((event) => {
        // write your code here
    });

#### ProcessEventEmitter.emit

Use `ProcessEventEmitter.emit(event)` for sending messages to all subscribers:

    new ProcessEventEmitter().emit({
        type: 'message',
        value: {
            body: 'Hello World!'
        }
    });

 
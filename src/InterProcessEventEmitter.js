import pm2 from 'pm2';
import cluster from 'cluster';

class InterProcessEventSubscription {

    /**
     * @param {InterProcessEventEmitter} emitter 
     */
    constructor(emitter) {
        this.emitter = emitter;
    }
    unsubscribe() {
        this.emitter.unsubscribe();
    }

}

function onClusterMessage(worker, message) {
    if (message && message.type === 'process.event') {
        if (cluster.workers) {
            Object.keys(cluster.workers).forEach(function (id) {
                const worker = cluster.workers[id];
                worker.send({
                    type: 'inter-process.event',
                    data: message.data,
                    id: message.id,
                    topic: message.topic
                });
            });
        }
    }
}

class InterProcessEventEmitter {

    static register() {
        cluster.removeListener('message', onClusterMessage);
        cluster.on('message', onClusterMessage);
    }
    
    listener;

    constructor() {
        //
    }

    /**
     * @param {*} data 
     */
    emit(data) {
        // if process is in cluster mode
        if (cluster.worker) {
            // and pm2 is being used
            if (process.env.pm_pid_path == null) {
                process.send({
                    type: 'process.event',
                    data: data,
                    id: new Date().getTime(),
                    topic: 'process'
                });
                return;
            }
        }
        process.emit('message', {
            type: 'process.event',
            data: data,
            id: new Date().getTime(),
            topic: 'process'
        });
    }

    /**
     * @param {*} next 
     */
    subscribe(next) {
        this.listener =  (message) => {
            if (message && message.type === 'process.event') {
                if (process.env.pm_pid_path != null) {
                    const newEvent = {
                        type: 'inter-process.event',
                        data: message.data,
                        id: message.id,
                        topic: message.topic
                    };
                    // use cached process list
                    if (this._procs) {
                        for (const proc of this._procs) {
                            pm2.sendDataToProcessId(proc, newEvent, (err, res) => {
                                //
                            });
                        }
                        return;
                    }
                    pm2.list((err, list) => {
                        const procs = list.filter((item) => item.name === process.env.name).map((item) => item.pm_id);
                        // cache process list
                        this._procs = procs;
                        for (const proc of procs) {
                            pm2.sendDataToProcessId(proc, newEvent, (err, res) => {
                                //
                            });
                        }
                    });
                } else {
                    next(message.data);
                }
            }
            if (message && message.type === 'inter-process.event') {
                // emit event
                next(message.data);
            }
        };
        process.on('message', this.listener);
        return new InterProcessEventSubscription(this);
    }

    unsubscribe() {
        if (this.listener) {
            process.removeListener('message', this.listener);
        }
    }

}

export {
    InterProcessEventSubscription,
    InterProcessEventEmitter
}
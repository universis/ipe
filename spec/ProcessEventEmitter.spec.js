import { ProcessEventEmitter } from '@universis/racket';
import { createServer } from 'http';
import { racket } from '../platform-server/src/service';

async function waitAsync(time) {
    return new Promise((resolve) => {
        setTimeout(resolve, 1000);
    });
}

describe('ProcessEventEmitter', () => {

    let serviceHost;
    beforeAll(async () => {
        const result = await new Promise((resolve, reject) => {
            const server = createServer(racket);
            server.on('listening', () => {
                return resolve(server);
            });
            server.on('error', (err) => {
                return reject(err);
            });
            server.listen(null, 'localhost');
        });
        const addressInfo = result.address();
        serviceHost = `http://${addressInfo.address}:${addressInfo.port}/`;
        console.log(`@universis/racket service host is running under ${serviceHost}`)
    });

    it('should create instance', () => {
        const emitter = new ProcessEventEmitter();
        expect(emitter).toBeInstanceOf(ProcessEventEmitter);
    });

    it('should try to subscribe', async () => {
        const emitter = new ProcessEventEmitter('http://127.0.0.1:13000');
        let errorThrown;
        emitter.subscribe((value) => {
            // do nothing
        }, (error) => {
            errorThrown = error;
        });
        await waitAsync(2000);
        expect(errorThrown).toBeInstanceOf(Error);
        expect(errorThrown.code).toEqual('ECONNREFUSED');
    });

    it('should subscribe', async () => {
        const emitter = new ProcessEventEmitter(serviceHost);
        let messages = [];
        let errorThrown;
        emitter.subscribe((message) => {
            messages.push(message);
        }, (error) => {
            errorThrown = error;
        });
        await waitAsync(2000);
        expect(errorThrown).toBeFalsy();
        expect(messages.length).toBeTruthy();
        messages = [];
        await new ProcessEventEmitter(serviceHost).emit({
            type: 'message',
            counter: 1
        });
        await waitAsync(2000);
        expect(messages.length).toBeTruthy();
        const message = messages[0];
        expect(message.counter).toEqual(1);
    });

    it('should unsubscribe', async () => {
        const emitter = new ProcessEventEmitter(serviceHost);
        let messages = [];
        let errorThrown;
        const subscription = emitter.subscribe((message) => {
            messages.push(message);
        }, (error) => {
            errorThrown = error;
        });
        await waitAsync(2000);
        expect(errorThrown).toBeFalsy();
        expect(messages.length).toBeTruthy();
        messages = [];
        await new ProcessEventEmitter(serviceHost).emit({
            type: 'message',
            counter: 1
        });
        await waitAsync(2000);
        expect(messages.length).toBeTruthy();
        const message = messages[0];
        expect(message.counter).toEqual(1);

        messages = [];
        subscription.unsubscribe();
        await new ProcessEventEmitter(serviceHost).emit({
            type: 'message',
            counter: 1
        });
        await waitAsync(2000);
        expect(messages.length).toBeFalsy();
    });
});
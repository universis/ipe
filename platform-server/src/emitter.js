import express from 'express';
import { v4 as uuidv4 } from 'uuid';

function emitter() {
    const router = express.Router();
    const clients = new Map();

    router.get('/subscribe', (req, res, next) => {
        const headers = {
            'Content-Type': 'text/event-stream',
            'Connection': 'keep-alive',
            'Cache-Control': 'no-store',
            'X-Accel-Buffering': 'no'
        };
        const key = uuidv4();
        clients.set(key, {
            channel: res,
            dateCreated: new Date()
        });
        res.writeHead(200, headers);
        res.write(`data: ${JSON.stringify({ id: key, type: 'connected'})}`);
        res.write('\n\n');
        // handle response close
        res.on('close', () => {
            clients.delete(key);
        });
    });

    router.post('/emit', (req, res, next) => {
        const message = req.body;
        for (const [k, client] of clients) {
            client.channel.write(`data: ${JSON.stringify(message)}`);
            client.channel.write('\n\n');
        }
        res.json({
            ok: true
        });
    });

    return router;
}

export {
    emitter
}
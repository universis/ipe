import { Server } from 'http';

export declare function serve(port?: any, host?: string): void;

export declare async function serveAsync(port?: any, host?: string): Promise<Server>;
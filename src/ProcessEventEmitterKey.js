import path from 'path';
import fs from 'fs';
import crypto from 'crypto';
import {mkdirp} from 'mkdirp';

class ProcessEventEmitterKey {
    /**
     * @returns {Promise<string[]>}
     */
    async read() {
        return new Promise((resolve, reject) => {
            // read or create key file
            const file = path.resolve(process.cwd(), '.cache' , '.racket');
            let keys = [];
            void fs.readFile(file, (err, data) => {
                if (err) {
                    if (err.code === 'ENOENT') {
                        // create .cache/.racket
                        keys = [
                            crypto.randomBytes(64).toString('base64')
                        ];
                        return mkdirp(path.resolve(process.cwd(), '.cache')).then(() => {
                            void fs.writeFile(file, keys.join('\n'), 'utf-8', (err) => {
                                if (err) {
                                    return reject(err);
                                }
                                return resolve(keys);
                            });
                        }).catch((err) => {
                            return reject(err);
                        });
                    }
                    return reject(err);
                }
                // get keys
                keys = data.toString().split('\n').filter((value) => value != null && value.length > 0);
                return resolve(keys);
            });
        });
    }

    /**
     * @returns {Promise<string>}
     */
    async readOne() {
        const keys = await this.read();
        return keys[0];
    }
}

export {
    ProcessEventEmitterKey
}
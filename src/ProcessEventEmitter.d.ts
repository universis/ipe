export declare class ProcessEventSubscription {
    constructor(emitter: { unsubscribe(): void });
    unsubscribe(): void;
}

export declare class ProcessEventEmitter<T> {

    constructor(serviceHost?: string);
    emit(value: T): Promise<void>;
    subscribe(next: (value: T) => void | Promise<void>, onError?: (error: Error) => void): ProcessEventSubscription;
    unsubscribe(): void;
    async ping(): Promise<boolean>;

}
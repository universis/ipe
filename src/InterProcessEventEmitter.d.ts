export declare class InterProcessEventSubscription {
    constructor(emitter: { unsubscribe(): void });
    unsubscribe(): void;
}

export declare class InterProcessEventEmitter<T> {

    constructor();
    emit(value: T): Promise<void>;
    subscribe(next: (value: T) => void | Promise<void>): InterProcessEventSubscription;
    unsubscribe(): void;

}
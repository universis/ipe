import express from 'express';
import { ProcessEventEmitterKey } from '../../src/ProcessEventEmitterKey';

/**
 * @param {import('express').Express} service 
 * @returns 
 */
function authenticate(service) {
    const router = express.Router();
    let keys = [];
    
    new ProcessEventEmitterKey().read().then((values) => {
        keys = values;
    });

    router.use((req, res, next) => {
        const reqKey =  req.query.key || req.headers['X-univ-api-key'];
        if (keys.indexOf(reqKey) >= 0) {
            return next();
        }
        return res.status(403).send('Unauthorized');
    });

    return router;
}

export {
    authenticate
}
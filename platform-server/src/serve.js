import { createServer } from 'http';
import { racket } from './service';
import debug from 'debug';
debug.enable('racket:*')

const error = debug('racket:error');
const log = debug('racket:log');

function normalizePort(val) {
    let port = parseInt(val, 10);
    if (isNaN(port)) {
        // named pipe
        return val;
    }
    if (port >= 0) {
        // port number
        return port;
    }
    return false;
}
/**
 * 
 * @param {*=} port
 * @param {string=} host
 * @returns void
 */
async function serve(port, host) {
    const server = createServer(racket);
    // get port from environment
    const serverPort = normalizePort(port != null ? port : 14400);
    // get bind address.
    const serverHost = host || '127.0.0.1';

    server.on('error', (err) => {
        error(err);
    });

    server.on('listening', () => {
        let addr = server.address();
        log(`Inter-process event server starts listening on http://${addr.address}:${addr.port}`);
    });
    server.listen(serverPort, serverHost);
}

/**
 * 
 * @param {*=} port
 * @param {string=} host
 * @returns 
 */
async function serveAsync(port, host) {
    return new Promise((resolve, reject) => {
        const server = createServer(racket);
        // get port from environment
        const serverPort = normalizePort(port != null ? port : 14400);
        // get bind address.
        const serverHost = host || '127.0.0.1';

        server.on('error', (err) => {
            error(err);
            reject(err);
        });

        server.on('listening', () => {
            let addr = server.address();
            log(`Inter-process event server starts listening on http://${addr.address}:${addr.port}`);
            resolve(server);
        });
        server.listen(serverPort, serverHost);
    });
}

export {
    serve,
    serveAsync
}
